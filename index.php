<?php

$isProduction = true;

// change the following paths if necessary
$yii=dirname(__FILE__).'/yii/yii.php';

$config = dirname(__FILE__) . ($isProduction ? '/protected/config/production.php' : '/protected/config/development.php');

// remove the following lines when in production mode
if (!$isProduction) {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
}
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', $isProduction ? 1 : 3);

require_once($yii);
Yii::createWebApplication($config)->run();

