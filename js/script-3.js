function updateIndexPage() {
    var windowWidth = $(document).width() - 20;
    var tableWidth = Math.min(windowWidth, 1240);

    var height = Math.ceil(tableWidth / 4);

	$('#index table.tiles td').map(function () {
		var node = $(this);

    	var isRect = node.hasClass('rect');
		if (isRect) {
			node.height(height);
			node.width(height * 2);
		}
		
		var isSquared = node.hasClass('squared');
		var multiplier = 1;
		if (node.hasClass('banner')) {
			multiplier = 2;
		}
		if (isSquared) {
			node.width(height * multiplier);
			node.height(height * multiplier);
			node.css('font-size', height / 20);
            node.find('.header').css('font-size', height / 17.5);
            node.find('.description').css('font-size', height / 21);
		}
		
		/*node.find('.arrow').map(function() {
			$(this).css('border-width', height / 10);
		});*/
	});

    $('#index table.tiles td.banner.squared a.square-wrapper').map(function () {
        var node = $(this);
        node.width(height * 2);
        node.height(height * 2);
    });

    $('#index table.tiles td.banner.rect a.square-wrapper').map(function () {
        var node = $(this);
        node.width(height * 2);
        node.height(height);
    });

    $('#index table.tiles td.banner.squared div').map(function () {
        var node = $(this);
        node.width(height * 2);
        node.height(height * 2);
    });

    $('#index table.tiles td.banner.rect div').map(function () {
        var node = $(this);
        node.width(height * 2);
        node.height(height);
    });
}

var images = new Array()
function preloadImages() {
    for (i = 0; i < preloadImages.arguments.length; i++) {
        images[i] = new Image()
        images[i].src = preloadImages.arguments[i]
    }
}

preloadImages(
    "css/images/index-1024/brick-1-hover.jpg",
    "css/images/index-1024/brick-2-hover.png",
    "css/images/index-1024/brick-3-hover.png",
    "css/images/planing-1024/floor-1-current.png",
    "css/images/planing-1024/floor-2-current.png",
    "css/images/planing-1024/floor-3-current.png",
    "css/images/planing-1024/floor-4-current.png",
    "css/images/planing-1024/floor-5-current.png",
    "css/images/planing-1024/floor-6-current.png",
    "css/images/planing-1024/floor-7-current.png",
    "css/images/planing-1024/floor-8-current.png"
);

$(function() {
    // Common
    $('a[href*=#]:not([href=#], .local-navigation-item)').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 500);
                return false;
            }
        }
    });

    $('a.local-navigation-item[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 47
                }, 500);
                return false;
            }
        }
    });

    // Index page
    $(window).resize(function() {
        updateIndexPage();
    });

	updateIndexPage();


});
