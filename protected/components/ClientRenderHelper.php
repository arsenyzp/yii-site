<?php

class ClientRenderHelper {
    static $textsByLabel = array();
    static $contactsByLabel = array();
    static $filesByLabel = array();
    static $isInitialized = false;

    protected static function initialize() {
        if (self::$isInitialized) {
            return;
        }

        // Init lazy cache
        $texts = TextExt::model()->findAll();
        $contacts = ContactExt::model()->findAll();
        $files = FileExt::model()->findAll();

        foreach ($texts as $text) {
            self::$textsByLabel[$text->label] = $text->text;
        }

        foreach ($contacts as $contact) {
            self::$contactsByLabel[$contact->label] = $contact->contact;
        }

        foreach ($files as $file) {
            self::$filesByLabel[$file->label] = array($file->file_filename, $file->download_name);
        }
    }

    public static function pageHref($action, $params = array()) {
        return Yii::app()->createUrl('client/' . $action, $params);
    }

    public static function ajaxHref($action, $params = array()) {
        return Yii::app()->createUrl('ajax/' . $action, $params);
    }

    public static function pageHeader() {
        return Yii::app()->getController()->getPageTitle();
    }

    public static function pageBase($isFull = false) {
        return Yii::app()->getBaseUrl($isFull);
    }

    public static function _($text, $limit = -1) {
//        $t = $text;
//        if ($limit != -1) {
//            if (mb_strlen($text) > $limit) {
//                $t = mb_substr($text, 0, $limit, Yii::app()->charset) . ' ...';
//            }
//        }
        return CHtml::encode($text);
    }

    public static function getContactByLabel($label) {
        self::initialize();

        if (!array_key_exists($label, self::$contactsByLabel)) {
            return "[WARNING! Contact Not Found! Label '$label']";
        }
        return self::_(self::$contactsByLabel[$label]);
    }

    public static function getTextByLabel($label) {
        self::initialize();

        if (!array_key_exists($label, self::$textsByLabel)) {
            return "[WARNING! Text Not Found! Label '$label']";
        }
        return self::$textsByLabel[$label];
    }

    public static function getUploadedFileByLabel($label) {
        self::initialize();

        if (!array_key_exists($label, self::$filesByLabel)) {
            return "[WARNING! File Not Found! Label '$label']";
        }
        return self::$filesByLabel[$label];
    }

    public static function eventTypeToClassName($type) {
        return $type == EventType::TYPE_REPORT ? 'report' : 'announce';
    }
}
