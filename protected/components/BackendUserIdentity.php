<?php

class BackendUserIdentity extends CUserIdentity
{
    protected $id;

    public static function hashPassword($password) {
        $salt = 'BNuir92vbmc984zx';
        $passwordHashed = sha1($password . $salt);
        return $passwordHashed;
    }

    public function authenticate() {
        $user = CmsAdminExt::model()->findByAttributes(
            array(
                'email' => strtolower($this->username),
                'password' => self::hashPassword($this->password)
            )
        );

        if ($user == NULL) {
            return false;
        }

        $this->id = $user->id;
        $this->setState('email', $user->email);
        return true;
    }

    public function getId() {
        return $this->id;
    }
}