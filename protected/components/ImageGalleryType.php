<?php

class ImageGalleryType {
    CONST TYPE_EVENTS = 1;
    CONST TYPE_FLOORS = 2;

    public static function toString($type) {
        $types = array(
            self::TYPE_EVENTS => 'События',
            self::TYPE_FLOORS => 'Этажи'
        );

        return $types[$type];
    }
}