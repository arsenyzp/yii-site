<?php

class AdminBackendController extends CController
{
    public $layout = 'admin';

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        // Workaround implementation of ip filter
        $clientIp = @$_SERVER['HTTP_CF_CONNECTING_IP'];
        $allowedIps = AllowedIpAddressExt::getAllowedIpsArray();
        if (!in_array($clientIp, $allowedIps)) {
            // $this->redirect(Yii::app()->createUrl('client/index'));
        }

        return array(
            // Allow access for authorized users
            array(
                'allow',
                'users' => array('@'),
            ),

            // Deny access for non-authorized users
            array(
                'deny',
                'users' => array('?'),
            ),
        );
    }
}
