<?php

class ResidentsController extends AdminBackendController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'number ASC, name ASC';

        $provider = new CActiveDataProvider('ResidentExt');
        $this->render(
            'index',
            array(
                'provider' => $provider,
                'criteria' => $criteria,
            )
        );
    }

    public function actionCreate()
    {
        $resident = new ResidentExt();
        $this->render(
            'edit',
            array(
                'model' => $resident,
                'floors' => CHtml::listData(FloorExt::model()->findAll(), 'id', 'number')
            )
        );
    }

    public function actionUpdate()
    {
        $resident = ResidentExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render(
            'edit',
            array(
                'model' => $resident,
                'floors' => CHtml::listData(FloorExt::model()->findAll(), 'id', 'number')
            )
        );
    }

    public function actionDelete()
    {
        ResidentExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('ResidentExt');
        $saver->update();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('ResidentExt');
        $saver->safeSaving = true;

        $saver->beforeRenderUpdate = function(VMEntitySaver $saver) {
            return array(
                'floors' => CHtml::listData(FloorExt::model()->findAll(), 'id', 'number')
            );
        };

        $saver->afterSave = function(ResidentExt $model) {
            VMUploadedMediaManager::quickSave($model, 'image');
            VMUploadedMediaManager::quickSave($model, 'occupied');
        };

        $saver->save();
    }
}