<?php

class FilesController extends AdminBackendController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'comment ASC';

        $provider = new CActiveDataProvider(
            'FileExt',
            array(
                'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=>50,
                ),
            )
        );
        $this->render('index', array('provider' => $provider));
    }

    public function actionCreate()
    {
        $file = new FileExt();
        $this->render('edit', array('model' => $file));
    }

    public function actionUpdate()
    {
        $file = FileExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render('edit', array('model' => $file));
    }

    public function actionDelete()
    {
        FileExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('FileExt');
        $saver->update();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('FileExt');
        $saver->safeSaving = true;

        $saver->afterSave = function(FileExt $model) {
            VMUploadedMediaManager::quickSave($model, 'file');
        };

        $saver->save();
    }
}