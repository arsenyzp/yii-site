<?php

class ContactsController extends AdminBackendController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'comment ASC';

        $provider = new CActiveDataProvider(
            'ContactExt',
            array(
                'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=>50,
                ),
            )
        );
        $this->render('index', array('provider' => $provider));
    }

    public function actionCreate()
    {
        $contact = new ContactExt();
        $this->render('edit', array('model' => $contact));
    }

    public function actionUpdate()
    {
        $contact = ContactExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render('edit', array('model' => $contact));
    }

    public function actionDelete()
    {
        ContactExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('ContactExt');
        $saver->update();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('ContactExt');
        $saver->safeSaving = true;

        $saver->save();
    }
}