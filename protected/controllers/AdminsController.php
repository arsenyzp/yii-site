<?php

class AdminsController extends AdminBackendController
{
    public function actionIndex()
    {
        $provider = new CActiveDataProvider('CmsAdminExt');
        $this->render('index', array('provider' => $provider));
    }

    public function actionCreate()
    {
        $cmsAdmin = new CmsAdminExt();
        $this->render('edit', array('model' => $cmsAdmin));
    }

    public function actionUpdate()
    {
        $cmsAdmin = CmsAdminExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render('edit', array('model' => $cmsAdmin));
    }

    public function actionDelete()
    {
        CountryExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('CmsAdminExt');
        $saver->update();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('CmsAdminExt');
        $saver->safeSaving = true;

        $saver->beforeSave = function(CmsAdminExt $object) {
            $object->password = BackendUserIdentity::hashPassword($object->password);
        };

        $saver->save();
    }
}