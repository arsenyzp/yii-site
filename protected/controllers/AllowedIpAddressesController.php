<?php

class AllowedIpAddressesController extends AdminBackendController
{
    public function actionIndex()
    {
        $provider = new CActiveDataProvider('AllowedIpAddressExt');
        $this->render('index', array('provider' => $provider));
    }

    public function actionCreate()
    {
        $allowedIpAddress = new AllowedIpAddressExt();
        $this->render('edit', array('model' => $allowedIpAddress));
    }

    public function actionUpdate()
    {
        $allowedIpAddress = AllowedIpAddressExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render('edit', array('model' => $allowedIpAddress));
    }

    public function actionDelete()
    {
        AllowedIpAddressExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('AllowedIpAddressExt');
        $saver->update();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('AllowedIpAddressExt');
        $saver->safeSaving = true;

        $saver->save();
    }
}