<?php

class FloorsController extends AdminBackendController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'number ASC';

        $provider = new CActiveDataProvider('FloorExt');
        $this->render(
            'index',
            array(
                'provider' => $provider,
                'criteria' => $criteria,
            )
        );
    }

    public function actionCreate()
    {
        $floor = new FloorExt();
        $this->render(
            'edit',
            array(
                'model' => $floor,
                'imageGalleries' => CHtml::listData(ImageGalleryExt::model()->findAllByAttributes(array('type' => ImageGalleryType::TYPE_FLOORS)), 'id', 'name')
            )
        );
    }

    public function actionUpdate()
    {
        $floor = FloorExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render(
            'edit',
            array(
                'model' => $floor,
                'imageGalleries' => CHtml::listData(ImageGalleryExt::model()->findAllByAttributes(array('type' => ImageGalleryType::TYPE_FLOORS)), 'id', 'name')
            )
        );
    }

    public function actionDelete()
    {
        FloorExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('FloorExt');
        $saver->update();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('FloorExt');
        $saver->safeSaving = true;

        $saver->beforeRenderUpdate = function(VMEntitySaver $saver) {
            return array(
                'imageGalleries' => CHtml::listData(ImageGalleryExt::model()->findAllByAttributes(array('type' => ImageGalleryType::TYPE_FLOORS)), 'id', 'name')
            );
        };

        $saver->afterSave = function(FloorExt $model) {
            VMUploadedMediaManager::quickSave($model, 'occupied');
            VMUploadedMediaManager::quickSave($model, 'background');
        };

        $saver->save();
    }
}