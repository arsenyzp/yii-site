<?php

class DashboardController extends AdminBackendController
{
    public function actionIndex()
    {
        $this->redirect(Yii::app()->createUrl('admins/index'));
    }

    public function actionError()
    {
        if ($error=Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                // echo $error['message'];
		// $this->redirect(Yii::app()->createUrl('client/index'));
            } else {
                 $this->render('error', $error);
		//$this->redirect(Yii::app()->createUrl('client/index'));
            }
        }
    }

    public function actionSignIn() {
        if (!Yii::app()->user->isGuest) {
            $this->redirect('index');
        }

        $model = new CmsAdminExt();
        $param = Yii::app()->request->getPost('CmsAdminExt');

        if ($param != NULL) {
            $signInForm = new SignInForm();
            $signInForm->attributes = $param;
            if ($signInForm->login()) {
                $this->redirect('index');
            }

            $model->email = array_key_exists('email', $param) ? $param['email'] : '';
        }

        $this->render('signIn', array('model' => $model));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect($this->createUrl('dashboard/signIn'));
    }

    public function accessRules() {
        // Workaround implementation of ip filter
        $clientIp = @$_SERVER['HTTP_CF_CONNECTING_IP'];
        $allowedIps = AllowedIpAddressExt::getAllowedIpsArray();
        if (!in_array($clientIp, $allowedIps)) {
            //$this->redirect(Yii::app()->createUrl('client/index'));
        }

        return array(
            // Allow access for non-authorized users with correct ip only to signIn
            array(
                'allow',
                'controllers' => array('dashboard'),
                'actions' => array('signIn', 'error'),
                'users' => array('?'),
            ),

            // Otherwise deny access for non-authorized users
            array(
                'deny',
                'users' => array('?'),
            ),

            // If page not blocked allow access
            array(
                'allow',
                'users' => array('@'),
            ),
        );
    }
}
