<?php

class TextExt extends Text
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            array(
                'id' => 'ID',
                'label' => 'Системное имя',
                'text' => 'Текст',
                'comment' => 'Комментарий к расположению',
            )
        );
    }
}