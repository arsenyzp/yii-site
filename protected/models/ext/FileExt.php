<?php

class FileExt extends File
{
    public $file;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            array(
                'id' => 'ID',
                'label' => 'Системное имя',
                'file' => 'Файл',
                'comment' => 'Комментарий к расположению',
                'download_name' => 'Название загрузки',
            )
        );
    }
}