<?php

class EventExt extends Event
{
    public $icon;
    public $banner;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getEventsByType($type, $count, $offset, $isImportant = false) {
        $criteria = new CDbCriteria;
        if ($type != EventType::TYPE_ANY) {
            $criteria->compare('type', $type);
        }
        if ($isImportant) {
            $criteria->compare('is_important', 1);
        }
        $criteria->order = 'rating > 0 DESC, rating ASC, date DESC';
        $criteria->limit = $count;
        $criteria->offset = $offset;
        $criteria->compare('is_shown', 1);

        return $this->findAll($criteria);
    }

    public function getThreeLatest($ignoredEventId, $isImportant = false) {
        $criteria = new CDbCriteria;
        $criteria->compare('is_shown', 1);
        $criteria->addNotInCondition('id', array($ignoredEventId));
        if ($isImportant) {
            $criteria->compare('is_important', 1);
        }
        $criteria->order = 'rating > 0 DESC, rating ASC, date DESC';

        return $this->findAll($criteria);
    }

    public function getLatest() {
        $criteria = new CDbCriteria;
        $criteria->compare('is_shown', 1);
        $criteria->order = 'rating > 0 DESC, rating ASC, date DESC LIMIT 4';

        return $this->findAll($criteria);
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            array(
                'id' => 'ID',
                'header' => 'Заголовок',
                'type' => 'Тип',
                'date' => 'Дата',
                'link' => 'Ссылка ВК',
                'link_fb' => 'Ссылка FB',
                'text_1' => 'Краткий текст',
                'text_2' => 'Развернутый текст',
                'image_gallery_id' => 'Галлерея',
                'banner' => 'Баннер',
                'icon' => 'Иконка',
                'is_shown' => 'Показывать посетителям',
                'is_important' => 'Важное событие',
                'vk_link_title' => 'Текст между ссылками facebook и vk',
                'rating' => 'Приоритет',
            )
        );
    }
}