<?php

class ImageExt extends Image
{
    public $image;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            array(
                'id' => 'ID',
                'image' => 'Изображение',
            )
        );
    }
}