<?php

class FloorExt extends Floor
{
    public $background;
    public $occupied;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
                array(
                'id' => 'ID',
                'number' => 'Этаж',
                'free_space' => 'Свободная площадь',
                'occupied_space' => 'Занятая площадь',
                'total_space' => 'Общая площадь',
                'background' => 'Планировка этажа (без арендаторов)',
                'occupied' => 'Планировка этажа (со всеми арендаторами)',
                'image_gallery_id' => 'Галлерея этажа',
                'image_height' => 'Высота картинки'
            )
        );
    }
}