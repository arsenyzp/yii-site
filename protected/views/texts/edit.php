<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('texts/save'),
        )
    );
?>

<h1>Добавить / Редактировать текстовую вставку</h1>

<hr />

<fieldset>
    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldRow($model, 'label', array('hint' => 'Уникальное системное имя (информация для программиста - не менять)', 'class' => 'span4')); ?>
    <?= $form->textareaRow($model, 'comment', array('hint' => 'Расположение на сайте', 'class' => 'span8', 'rows' => 2, 'style' => 'resize: none')); ?>
    <?= $form->textareaRow($model, 'text', array('hint' => 'Текст вставки', 'class' => 'span8', 'rows' => 10, 'style' => 'resize: none')); ?>

    <?= $form->hiddenField($model, 'id'); ?>

    <div class="form-actions">
        <?php
            $this->widget(
                'TbButton',
                array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => 'Сохранить'
                )
            );
        ?>

        <?php
            $this->widget(
                'TbButton',
                array(
                    'buttonType' => 'link',
                    'url' => $this->createUrl('texts/index'),
                    'label' => 'Отмена'
                )
            );
        ?>
    </div>
</fieldset>

<?php $this->endWidget(); ?>