<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('residents/save'),
        )
    );
?>
    <fieldset>
        <legend>Добавить / Редактировать резидента</legend>

        <?= $form->errorSummary($model); ?>
        <?= $form->textFieldRow($model, 'number', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'name', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'site', array('class' => 'span3')); ?>
        <?= $form->dropDownListRow($model, 'floor_id', $floors, array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'offset_left', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'offset_right', array('class' => 'span3')); ?>
        <?= $form->textareaRow($model, 'short_description', array('class'=>'span8', 'rows' => 5)); ?>
        <hr />
        <?= $form->fileFieldRow($model, 'image', array('class' => 'span3')); ?>
        <?= $form->fileFieldRow($model, 'occupied', array('class' => 'span3')); ?>
        <?= $form->textareaRow($model, 'region', array('class'=>'span8', 'rows' => 5)); ?>

        <?= $form->hiddenField($model, 'id'); ?>

        <div class="form-actions">
            <?php
                $this->widget(
                    'TbButton',
                    array(
                        'buttonType' => 'submit',
                        'type' => 'primary',
                        'label' => 'Сохранить'
                    )
                );
            ?>

            <?php
                $this->widget(
                    'TbButton',
                    array(
                        'buttonType' => 'link',
                        'url' => $this->createUrl('residents/index'),
                        'label' => 'Отмена'
                    )
                );
            ?>
        </div>
    </fieldset>
<?php $this->endWidget(); ?>