<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('imageGalleries/save'),
        )
    );
?>
    <fieldset>
        <legend>Добавить / Редактировать галерею</legend>

        <?= $form->errorSummary($model); ?>
        <?= $form->textFieldRow($model, 'name', array('class' => 'span3')); ?>

        <?= $form->hiddenField($model, 'type'); ?>
        <?= $form->hiddenField($model, 'id'); ?>

        <div class="form-actions">
            <?php
                $this->widget(
                    'TbButton',
                    array(
                        'buttonType' => 'submit',
                        'type' => 'primary',
                        'label' => 'Сохранить'
                    )
                );
            ?>

            <?php
                $this->widget(
                    'TbButton',
                    array(
                        'buttonType' => 'link',
                        'url' => $this->createUrl('imageGalleries/index', array('type' => $model->type)),
                        'label' => 'Отмена'
                    )
                );
            ?>
        </div>
    </fieldset>
<?php $this->endWidget(); ?>