<?php
    $floorNo = $floor->number;
    $isDefaultPlaning = false;
?>
<div id="planing">
    <div id="floors-navbar" class="floors-absolute">
        <div class="floors">
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 1)) ?>#floor-planing" class="soft-hover floor top <?= $floorNo == 1 ? 'current' : '' ?>" style="padding-right:22px;">цоколь</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 2)) ?>#floor-planing" class="soft-hover floor number-2 <?= $floorNo == 2 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 3)) ?>#floor-planing" class="soft-hover floor number-3 <?= $floorNo == 3 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 4)) ?>#floor-planing" class="soft-hover floor number-4 <?= $floorNo == 4 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 5)) ?>#floor-planing" class="soft-hover floor number-5 <?= $floorNo == 5 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 6)) ?>#floor-planing" class="soft-hover floor number-6 <?= $floorNo == 6 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 7)) ?>#floor-planing" class="soft-hover floor number-7 <?= $floorNo == 7 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 8)) ?>#floor-planing" class="soft-hover floor number-8 <?= $floorNo == 8 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => $floorNo)) ?>#planing" class="soft-hover floor top">фасад</a>
        </div>
    </div>

    <div class="building">
        <div class="building-center">
        <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 8)) ?>#floor-planing" class="soft-hover floor-selection floor-selection-8"></a>
        <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 7)) ?>#floor-planing" class="soft-hover floor-selection floor-selection-7"></a>
        <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 6)) ?>#floor-planing" class="soft-hover floor-selection floor-selection-6"></a>
        <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 5)) ?>#floor-planing" class="soft-hover floor-selection floor-selection-5"></a>
        <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 4)) ?>#floor-planing" class="soft-hover floor-selection floor-selection-4"></a>
        <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 3)) ?>#floor-planing" class="soft-hover floor-selection floor-selection-3"></a>
        <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 2)) ?>#floor-planing" class="soft-hover floor-selection floor-selection-2"></a>
        <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 1)) ?>#floor-planing" class="soft-hover floor-selection floor-selection-1"></a>
        </div>
    </div>
    <div id="floor-planing" class="floor-planing <?= $isDefaultPlaning ? 'default' : '' ?>">
        <div class="floors">
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 1)) ?>#floor-planing" class="soft-hover floor top <?= $floorNo == 1 ? 'current' : '' ?>" style="padding-right:22px;">цоколь</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 2)) ?>#floor-planing" class="soft-hover floor number-2 <?= $floorNo == 2 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 3)) ?>#floor-planing" class="soft-hover floor number-3 <?= $floorNo == 3 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 4)) ?>#floor-planing" class="soft-hover floor number-4 <?= $floorNo == 4 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 5)) ?>#floor-planing" class="soft-hover floor number-5 <?= $floorNo == 5 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 6)) ?>#floor-planing" class="soft-hover floor number-6 <?= $floorNo == 6 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 7)) ?>#floor-planing" class="soft-hover floor number-7 <?= $floorNo == 7 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => 8)) ?>#floor-planing" class="soft-hover floor number-8 <?= $floorNo == 8 ? 'current' : '' ?>">&nbsp;</a>
            <a href="<?= ClientRenderHelper::pageHref('planing', array('id' => $floorNo)) ?>#planing" class="soft-hover floor top">фасад</a>
        </div>

        <div class="floor-background" style="width: 1024px!important; height: <?= ClientRenderHelper::_($floor->image_height) ?>px">
            <div class="floor-background-image-wrapper" style="width: 1024px; height: <?= ClientRenderHelper::_($floor->image_height) ?>px">
                <map name="floor" id="floor" style="z-index: 100; width: 100%; height: <?= ClientRenderHelper::_($floor->image_height) ?>px">
                    <?php
                    foreach($floor->residents as $resident) {
                        if (!$resident->region)
                            continue;
                        ?>
                        <area data-resident-id="<?= ClientRenderHelper::_($resident->id) ?>" shape="poly" href="#floor-planing" coords="<?= $resident->region ?>" style="cursor: pointer; width: 100%; height: <?= ClientRenderHelper::_($floor->image_height) ?>px">
                    <?php } ?>
                </map>
                <img src="<?= ClientRenderHelper::_($floor->background_filename) ?>" style="left: -108px; height: <?= ClientRenderHelper::_($floor->image_height) ?>px" />
            </div>

            <div style="position: relative; left: -108px; top: -<?= ClientRenderHelper::_($floor->image_height) ?>px">
                <div class="floor-background-image-wrapper">
                    <img style="z-index:100" usemap="#floor" src="<?= ClientRenderHelper::_($floor->occupied_filename) ?>" />
                </div>

                <?php foreach($floor->residents as $resident) { ?>
                    <?php if (!empty($resident->occupied_filename)) { ?>
                        <div class="floor-background-image-wrapper">
                            <img id="resident-image-<?= ClientRenderHelper::_($resident->id) ?>" class="resident-image" data-resident-id="<?= ClientRenderHelper::_($resident->id) ?>" src="<?= ClientRenderHelper::_($resident->occupied_filename) ?>" style="top: <?= ClientRenderHelper::_($resident->offset_right) ?>px; left: <?= ClientRenderHelper::_($resident->offset_left) ?>px" />
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>

        <div class="space">
            <span class="free-space">
                Свободно — <?= ClientRenderHelper::_($floor->free_space) ?>м<sup>2</sup>
            </span>
            <span class="occupied-space">
                Занято — <?= ClientRenderHelper::_($floor->occupied_space) ?>м<sup>2</sup>
            </span>
            <span class="total-space">
                Общая площадь — <?= ClientRenderHelper::_($floor->total_space) ?>м<sup>2</sup>
            </span>
            <br class="clear" />
        </div>
    </div>

    <div id="images-carousel" class="images-list owl-carousel" style="min-width: 1024px">
        <?php
            foreach ($floor->imageGallery->images as $image) {
        ?>
            <a class="magnific-image" href="<?= $image->image_filename ?>">
                <img src="css/images/1x1.png" style="background-image: url('<?= $image->image_filename ?>')" />
            </a>
        <?php
            }
        ?>
    </div>

    <div id="residents" class="residents">
        <ul id="residents-carousel" class="owl-carousel">
            <?php
                foreach ($floor->residents as $resident) {
            ?>
            <li class="resident" data-resident-id="<?= ClientRenderHelper::_($resident->id) ?>">
                <div class="image" style="background-image: url('<?= ClientRenderHelper::_($resident->image_filename) ?>')"></div>
                <div class="name"><?= ClientRenderHelper::_($resident->name) ?></div>
                <div class="site">
                    <?php if($resident->site) { ?>
                        <a href="http://<?= ClientRenderHelper::_($resident->site) ?>" target="_blank">
                            <?= ClientRenderHelper::_($resident->site) ?>
                        </a>
                    <?php } ?>
                </div>
                <div class="short-text"><?= ClientRenderHelper::_($resident->short_description) ?></div>
                <div class="number"><?= ClientRenderHelper::_($resident->number) ?></div>
            </li>
            <?php
                }
            ?>
        </ul>
    </div>

    <div class="documents">
        <div class="pdfs">
            <?php
            $download1 = ClientRenderHelper::getUploadedFileByLabel('contacts_application_rent_1');
            $download2 = ClientRenderHelper::getUploadedFileByLabel('contacts_application_event_1');
            $download3 = ClientRenderHelper::getUploadedFileByLabel('contacts_application_collaboration_1');
            $download4 = ClientRenderHelper::getUploadedFileByLabel('contacts_presentation_loft_1');
            $download5 = ClientRenderHelper::getUploadedFileByLabel('contacts_presentation_place_1');
            $download6 = ClientRenderHelper::getUploadedFileByLabel('contacts_presentation_members_1');
            ?>
            <a href="<?= $download1[0] ?>" download="<?= $download1[1] ?>" class="pdf-black">заявка<br />на аренду</a>
            <!--<a href="<?= $download2[0] ?>" download="<?= $download2[1] ?>" class="pdf-black">Краткосрочная<br /> аренда</a>-->
            <a href="<?= $download3[0] ?>" download="<?= $download3[1] ?>" class="pdf-black">заявка на<br />событие</a>
            <a href="<?= $download4[0] ?>" download="<?= $download4[1] ?>" class="pdf-orange">Презентация<br /> проекта.PDF</a>
            <!--<br />

            <a href="<?= $download5[0] ?>" download="<?= $download5[1] ?>" class="pdf-orange">Презентация<br /> площадки.PDF</a>
            <a href="<?= $download6[0] ?>" download="<?= $download6[1] ?>" class="pdf-orange">Презентация<br /> галереи.PDF</a>-->
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#images-carousel").owlCarousel({
            items: 5,
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1]
        });

        var residentsIdx = {
            <?php
                $cnt = 0;
                foreach($floor->residents as $resident) {
                    echo $resident->id . ':' . $cnt . ',';
                    $cnt++;
                }
            ?>
        };
        $("#residents-carousel").owlCarousel({
            items: 4,
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1],
            paginationNumbers: true
        });
        var residentsOwl = $("#residents-carousel").data('owlCarousel');

        $('.magnific-image').magnificPopup(
            {
                type: 'image',
                gallery: {
                    enabled: true
                }
            }
        );

        function clearSelectedResidents(residentId) {
            $('img.resident-image').each(function() {
                var currentNode = $(this);
                currentNode.removeClass('current');
                if (currentNode.attr('data-resident-id') == residentId) {
                    currentNode.addClass('current');
                }
            });
            $('li.resident').each(function() {
                var currentNode = $(this);
                currentNode.removeClass('current');
                if (currentNode.attr('data-resident-id') == residentId) {
                    currentNode.addClass('current');
                }
            });
        }

        function scrollToResidents() {
            $('html,body').animate({
                scrollTop: $('#residents').offset().top
            }, 500);
        }

        function scrollToPlanning(residentId) {
            $('html,body').animate({
                scrollTop: $('#resident-image-' + residentId).offset().top - 100
            }, 500);
        }

        $('area').each(function () {
            $(this).click(function(e) {
                var residentId = $(this).attr('data-resident-id');
                clearSelectedResidents(residentId);
                scrollToResidents();
                residentsOwl.jumpTo(residentsIdx[residentId]);
                e.stopPropagation();
                return false;
            });
        });

        $('area').each(function () {
            $(this).mouseover(function(e) {
                var residentId = $(this).attr('data-resident-id');
                $('#resident-image-' + residentId).addClass('current');
            });
            $(this).mouseout(function(e) {
                var residentId = $(this).attr('data-resident-id');
                $('#resident-image-' + residentId).removeClass('current');
            });
        });

        $('li.resident').each(function () {
            $(this).click(function() {
                var residentId = $(this).attr('data-resident-id');
                clearSelectedResidents(residentId);
                scrollToPlanning(residentId);
            });
        });

        $('a.floor-selection').each(function() {
            var node = $(this);
            node.click(function (e) {
                $('html,body').animate(
                    {
                        scrollTop: $('#floor-planing').offset().top
                    },
                    500,
                    function() {
                        window.location = node.attr('href');
                    }
                );

                e.stopPropagation();
                return false;
            });
        });

        // Navigation bar
        function updateLocalNavigation() {
            if ($(window).scrollTop() > 830 && $(window).scrollTop() < 1800) {
                $('#floors-navbar').show();
            } else {
                $('#floors-navbar').hide();
            }
        }

        $('.floors-absolute').width($('#planing').width());

        $(window).scroll(function() {
            updateLocalNavigation();
        });
        updateLocalNavigation();
    });
</script>
