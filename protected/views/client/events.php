<div id="local-navigation" class="local-navigation absolute">
    <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_ANY)) ?>">все</a>
    <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_ANNOUNCE)) ?>">будет</a>
    <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_NOW)) ?>">сейчас</a>
    <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_REPORT)) ?>">было</a>
    <!--<a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_IMPORTANT)) ?>">важные</a>-->
</div>

<div id="event">
    <div class="local-navigation">
        <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_ANY)) ?>">все</a>
        <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_ANNOUNCE)) ?>">будет</a>
        <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_NOW)) ?>">сейчас</a>
        <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_REPORT)) ?>">было</a>
        <!--<a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_IMPORTANT)) ?>">важные</a>-->
    </div>

    <?php
        $count = 0;
        foreach ($events as $event) {
            if ($count == 4) {
                $count = 0;
            }
    ?>
    <?php if ($count == 0) { ?>
        <div id="events-carousel" class="other-news" style="text-align: left; margin-top: 3px; padding-left: 8px">
    <?php } ?>
            <a href="<?= ClientRenderHelper::pageHref('event', array('id' => $event->id)) ?>" class="news" style="text-align: center">
                <div class="image image-1" style="background-image: url('<?= $event->icon_filename ?>')">
                    <div class="arrow-top"></div>
                </div>
                <div class="header">
                    <?= ClientRenderHelper::_($event->header) ?>
                </div>
                <div class="date">
                    <?= $event->type == EventType::TYPE_NOW ? 'до ' : '' ?>
                    <?= date('j', strtotime($event->date)); ?>
                    <?= EventType::getMothName(date('n', strtotime($event->date)) - 1); ?>
                    <?= date('Y', strtotime($event->date)); ?> г.
                </div>
                <div class="description">
                    <?= ClientRenderHelper::_($event->text_1, 25) ?>
                </div>
                <div class="type <?= ClientRenderHelper::eventTypeToClassName($event->type) ?>">
                    <?= EventType::eventTypeToString($event->type) ?>
                </div>
            </a>
    <?php $count++; ?>
    <?php if ($count == 4) { ?>
        </div>
    <?php } ?>
    <?php
        }
    ?>

    <?php if ($count != 4) { ?>
    </div>
    <?php } ?>

    <div id="news-add"></div>
</div>

<div style="text-align: center; margin-bottom: 40px;">
    <span id="more-news">
        Еще новости
    </span>
</div>

<script>
    var noMoreResults = <?= $noMoreResults; ?>;
    var offset = <?= $offset; ?>;

    $(function() {
        function updateLocalNavigation() {
            if ($(window).scrollTop() > 180) {
                $('#local-navigation').show();
            } else {
                $('#local-navigation').hide();
            }
        }

        $(window).scroll(function() {
            updateLocalNavigation();
        });
        updateLocalNavigation();

        if (noMoreResults) {
            $('#more-news').hide();
        }

        $('#more-news').click(
            function() {
                $.ajax({
                    url: "<?= ClientRenderHelper::ajaxHref('moreNews', array('id' => $type)) ?>",
                    data: {
                        'offset': offset
                    },
                    cache: false,
                    dataType: 'html'
                })
                .done(function(html) {
                        var htmlNode = $(html);
                        recievedNews = htmlNode.find('span.count').data('count');
                        offset += recievedNews;
                        if (recievedNews < 4) {
                            $('#more-news').hide();
                        }
                        $('#news-add').append(htmlNode);
                });
            }
        );
    });
</script>