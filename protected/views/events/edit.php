<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('events/save'),
        )
    );
?>
    <fieldset>
        <legend>Добавить / Редактировать событие</legend>

        <?= $form->errorSummary($model); ?>
        <?= $form->textFieldRow($model, 'header', array('class' => 'span3')); ?>
        <?= $form->dropDownListRow($model, 'type', EventType::getEventTypesList(), array('class' => 'span3')); ?>
        <?= $form->toggleButtonRow($model, 'is_shown', array('class' => 'span3')); ?>
        <?= $form->toggleButtonRow($model, 'is_important', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'rating', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'link', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'link_fb', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'vk_link_title', array('class' => 'span3')); ?>

        <?=
            $form->datepickerRow(
                $model,
                'date',
                array(
                    'class' => 'span3',
                    'options' => array(
                        'format' => 'yyyy-mm-dd',
                        'language' => 'ru',
                    )
                )
            );
        ?>

        <hr />
        <?= $form->fileFieldRow($model, 'icon', array('class' => 'span3')); ?>
        <?= $form->fileFieldRow($model, 'banner', array('class' => 'span3')); ?>
        <?= $form->dropDownListRow($model, 'image_gallery_id', $imageGalleries, array('class' => 'span3')); ?>
        <hr />
        <?= $form->textareaRow($model, 'text_1', array('class'=>'span8', 'rows' => 5)); ?>
        <hr />
        <?= $form->textareaRow($model, 'text_2', array('class'=>'span8', 'rows' => 20)); ?>

        <?= $form->hiddenField($model, 'id'); ?>

        <div class="form-actions">
            <?php
                $this->widget(
                    'TbButton',
                    array(
                        'buttonType' => 'submit',
                        'type' => 'primary',
                        'label' => 'Сохранить'
                    )
                );
            ?>

            <?php
                $this->widget(
                    'TbButton',
                    array(
                        'buttonType' => 'link',
                        'url' => $this->createUrl('events/index'),
                        'label' => 'Отмена'
                    )
                );
            ?>
        </div>
    </fieldset>
<?php $this->endWidget(); ?>