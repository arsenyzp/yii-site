<div style="margin: 0 auto; width: 350px">
    <?php
        $form = $this->beginWidget(
            'TbActiveForm',
            array(
                'id' => 'user',
                'htmlOptions' => array('class' => 'well'),
            )
        );
    ?>

    <?= $form->textFieldRow($model, 'email', array('class' => 'span3')); ?>
    <?= $form->passwordFieldRow($model, 'password', array('class' => 'span3')); ?>
    <br />
    <?php
        $this->widget(
            'TbButton',
            array(
                'buttonType' => 'submit',
                'label'=>'Login'
            )
        );
    ?>
    <?php $this->endWidget(); ?>
</div>